#!/usr/bin/python3

import csv
import sys
from xml.dom import minidom


# Before using this, it is necessary to
# open the file account_COUNTRY.ods
# and save a copy as a CSV

# what type of entity are we working on?
entity_code = "C"
entity_type = "Company"

country_specs = {
    "al": {
        # the full account code has 4 digits
        # any account code shorter than 4 digits is a group
        "full_code_length": 4,
        "languages": { "en":"SME", "sq":"NVM" }
    },
    "ch": {
        # the full account code has 4 digits
        # any account code shorter than 4 digits is a group
        "full_code_length": 4,
        # FIXME - add Romansch
        "languages": { "en":"SME", "fr":"PMU", "de":"KMU", "it":"PMI", "pt":"PME", "es":"PYME", "sq":"NVM" }
    }
}

country_spec = None
country_code = None

inc_bal_detail_mappings = { "A":"balance_sheet", "L":"balance_sheet" }
#inc_bal_detail_mappings = { "A":"assets", "I":"revenue", "E":"expense" }

def set_name(doc, e, name):
    e_field = doc.createElement("field")
    e_field.setAttribute("name", "name")
    e_field.appendChild(doc.createTextNode(name))
    e.appendChild(e_field)

def set_seq(doc, e, next_seq):
    e_field = doc.createElement("field")
    e_field.setAttribute("name", "sequence")
    e_field.setAttribute("eval", str(next_seq))
    e.appendChild(e_field)
    return next_seq + 10

def set_statement(doc, e, inc_bal):
    e_field = doc.createElement("field")
    e_field.setAttribute("name", "statement")
    if len(inc_bal) < 1:
        e_field.setAttribute("eval", "None")
    elif "B" == inc_bal:
        e_field.appendChild(doc.createTextNode("balance"))
    elif "I" == inc_bal:
        e_field.appendChild(doc.createTextNode("income"))
    e.appendChild(e_field)

def set_field_child(doc, e, name, value):
    if len(value) == 0:
        return
    e_field = doc.createElement("field")
    e_field.setAttribute("name", name)
    e_field.appendChild(doc.createTextNode(value))
    e.appendChild(e_field)

def set_code(doc, e, code):
    if len(code) == 0:
        return
    set_field_child(doc, e, "code", code)

def set_kind(doc, e, kind):
    set_field_child(doc, e, "kind", kind)

def set_ref(doc, e, name, value):
    if len(value) == 0:
        return
    e_field = doc.createElement("field")
    e_field.setAttribute("name", name)
    e_field.setAttribute("ref", value)
    e.appendChild(e_field)

def set_type(doc, e, type):
    set_ref(doc, e, "type", type)

def set_parent(doc, e, parent_id):
    set_ref(doc, e, "parent", parent_id)

def set_eval(doc, e, name, value):
    e_field = doc.createElement("field")
    e_field.setAttribute("name", name)
    e_field.setAttribute("eval", str(value))
    e.appendChild(e_field)

def set_receivable(doc, e):
    set_eval(doc, e, "receivable", True)

def set_payable(doc, e):
    set_eval(doc, e, "payable", True)

def set_stock(doc, e):
    set_eval(doc, e, "stock", True)

def set_party_required(doc, e):
    set_eval(doc, e, "party_required", True)

def set_reconcile(doc, e):
    set_eval(doc, e, "reconcile", True)

def set_deferral(doc, e):
    set_eval(doc, e, "deferral", True)

def get_csv_field_bool(colidx, row, field_name):
    return ("Y" == (row[colidx[field_name]].upper()))

# Program entry point / start here
if __name__ == '__main__':
    parents = []
    parent_ids = []
    tmplmap = {}
    acctmap = {}
    docs = {}
    templates = {}
    accounts = {}

    if len(sys.argv) < 2:
        print("Please specify country code")
        exit(1)
    country_code = sys.argv[1]
    print("Country: %s" % (country_code,))
    if country_code not in country_specs.keys():
        print("Country code not supported");
        exit(1)
    country_spec = country_specs[country_code]
    csv_filename = ("chart_%s.csv" % (country_code,))

    # CSV exported from the file swiss-sme-kmu-pme.ods
    with open(csv_filename, "rt") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=",")
        read_header = True
        colidx = {}
        for ln in country_spec["languages"].keys():
            doc = minidom.Document()
            docs[ln] = doc
            templates[ln] = []
            accounts[ln] = []
            next_seq = 10
        for row in csv_reader:
            if read_header:
                i = 0
                for col in row:
                    colidx[col] = i
                    i = i + 1
                read_header = False
            else:
                acct_code = row[colidx["Code"]]
                acct_type = row[colidx["Type"]]
                acct_entity = row[colidx["Entity"]]
                if acct_entity not in [ "*", entity_code ]:
                    continue
                tmpl_id = row[colidx["TemplateID"]]
                acct_id = row[colidx["AcctID"]]
                acct_inc_bal = row[colidx["IncomeOrBalance"]].upper()
                acct_inc_bal_detail = row[colidx["AssetLiabilityIncomeExpense"]].upper()
                acct_inventory = get_csv_field_bool(colidx, row, "Inventory")
                acct_subtype = row[colidx["Subtype"]].upper()
                acct_deferral = get_csv_field_bool(colidx, row, "Deferral")
                acct_party_required = get_csv_field_bool(colidx, row, "PartyRequired")
                acct_reconcile = get_csv_field_bool(colidx, row, "Reconcile")
                acct_create_template = get_csv_field_bool(colidx, row, "CreateTemplate")
                acct_create_instance = get_csv_field_bool(colidx, row, "CreateInstance")
                print("processing: code: %s tmpl_id: %s acct_id: %s" % (acct_code, tmpl_id, acct_id))
                if len(tmpl_id) == 0:
                    tmpl_id = "accttmpl_%s" % (acct_code,)
                    acct_id = "%s" % (acct_code,)
                if len(acct_code) == 0:
                    acct_id = "root_LN"
                else:
                    tmplmap[acct_code] = tmpl_id
                    acctmap[acct_code] = acct_id
                my_parent_code = ""
                my_parent_id = None
                if len(acct_code) < country_spec["full_code_length"]:
                    _parents = []
                    _parent_ids = []
                    for p in parents:
                        if len(p) < len(acct_code):
                            _parents.append(p)
                            if len(p) == 0:
                                _parent_ids.append("root_LN")
                            else:
                                _parent_ids.append(acctmap[p])
                    parents = _parents
                    parent_ids = _parent_ids
                    if len(parents) > 0:
                        my_parent_code = parents[-1]
                        my_parent_id = parent_ids[-1]
                    parents.append(acct_code)
                    parent_ids.append(acct_id)
                    print("updated parents: %s - %s" % (":".join(parents), ":".join(parent_ids)))
                else:
                    if len(parents) > 0:
                        my_parent_code = parents[-1]
                        my_parent_id = parent_ids[-1]
                my_parent_tmplid = None
                root_tmpl_id = ("%s_LN" % (country_code,))  # FIXME - this is a hack
                if len(my_parent_code) > 0:   # FIXME - another condition?
                    my_parent_tmplid = tmplmap[my_parent_code]
                elif len(acct_code) > 0:
                    my_parent_tmplid = root_tmpl_id  # FIXME - this is a hack
                print("my parent: %s/%s me: %s/%s" % (my_parent_code, my_parent_tmplid, acct_code, tmpl_id))
                for ln in country_spec["languages"].keys():
                    _tmpl_prefix = ("%s_%s" % (country_code, ln))
                    _tmpl_id = tmpl_id
                    if _tmpl_id == root_tmpl_id:
                        _tmpl_id = ("%s_%s" % (country_code, ln))
                    else:
                        _tmpl_id = ("%s_%s" % (_tmpl_prefix, _tmpl_id))
                    if acct_create_template:
                        record_template = docs[ln].createElement("record")
                        record_template.setAttribute("model", "account.account.type.template")
                        record_template.setAttribute("id", _tmpl_id)
                        tmpl_name = row[colidx[ln]]
                        if acct_type == "T":
                            tmpl_name = "Account Type Chart (%s)" % (tmpl_name,)
                        set_name(docs[ln], record_template, tmpl_name)
                        next_seq = set_seq(docs[ln], record_template, next_seq)
                        #set_statement(docs[ln], record_template, acct_inc_bal)
                        inc_bal = inc_bal_detail_mappings.get(acct_inc_bal_detail)
                        if inc_bal != None:
                            set_eval(docs[ln], record_template, inc_bal, True)
                        #if acct_subtype == "AR":
                        #    set_receivable(docs[ln], record_template)
                        #if acct_subtype == "AP":
                        #    set_payable(docs[ln], record_template)
                        #if acct_inventory:
                        #    set_stock(docs[ln], record_template)
                        templates[ln].append(record_template)
                        if my_parent_tmplid is not None:
                            _my_parent_tmplid = my_parent_tmplid
                            if _my_parent_tmplid == root_tmpl_id:
                                _my_parent_tmplid = ("%s_%s" % (country_code, ln))
                            else:
                                _my_parent_tmplid = ("%s_%s" % (_tmpl_prefix, _my_parent_tmplid))
                            set_parent(docs[ln], record_template, _my_parent_tmplid)
                    if acct_create_instance:
                        record_acct = docs[ln].createElement("record")
                        record_acct.setAttribute("model", "account.account.template")
                        _acct_id = acct_id
                        if _acct_id == "root_LN":
                            _acct_id = ("root_%s" % (ln,))
                        else:
                            _acct_id = ("%s_%s" % (_acct_id, ln))
                        record_acct.setAttribute("id", _acct_id)
                        acct_name = row[colidx[ln]]
                        if acct_type == "T":
                            acct_name = "Account Chart (%s)" % (acct_name,)
                        if acct_type == "A":
                            if acct_subtype == "AR":
                                set_kind(docs[ln], record_acct, "receivable")
                            elif acct_subtype == "AP":
                                set_kind(docs[ln], record_acct, "payable")
                            elif acct_inventory:
                                set_kind(docs[ln], record_acct, "stock")
                            elif acct_inc_bal_detail == "I":
                                set_kind(docs[ln], record_acct, "revenue")
                            elif acct_inc_bal_detail == "E":
                                set_kind(docs[ln], record_acct, "expense")
                            else:
                                set_kind(docs[ln], record_acct, "other")
                        else:
                            set_kind(docs[ln], record_acct, "view")
                        set_name(docs[ln], record_acct, acct_name)
                        set_code(docs[ln], record_acct, acct_code)
                        if len(acct_code) == 0:
                            set_type(docs[ln], record_acct, _tmpl_id)
                        if len(acct_code) == country_spec["full_code_length"] or acct_type == "A":
                            set_type(docs[ln], record_acct, _tmpl_id)
                        if acct_party_required:
                            set_party_required(docs[ln], record_acct)
                        if my_parent_id is not None:
                            _my_parent_id = my_parent_id
                            if _my_parent_id == "root_LN":
                                _my_parent_id = ("root_%s" % (ln,))
                            else:
                               _my_parent_id = ("%s_%s" % (_my_parent_id, ln))
                            set_parent(docs[ln], record_acct, _my_parent_id)
                        if acct_reconcile:
                            set_reconcile(docs[ln], record_acct)
                        if acct_deferral:
                            set_deferral(docs[ln], record_acct)
                        accounts[ln].append(record_acct)

    for ln in country_spec["languages"]:
        doc = docs[ln]
        e_tryton = doc.createElement("tryton")
        doc.appendChild(e_tryton)

        e_data = doc.createElement("data")
        e_data.setAttribute("language", ln)
        for t in templates[ln]:
            e_data.appendChild(t)
        for a in accounts[ln]:
            e_data.appendChild(a)

        e_tryton.appendChild(e_data)

        output_filename = "%s/account_%s_%s.xml" % (country_code, country_code, ln)
        with open(output_filename, "w") as f:
            f.write(doc.toprettyxml(indent="\t"))

