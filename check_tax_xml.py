#!/usr/bin/env python3

input_file = "tax_al.xml"


import libxml2
import sys

nodes = {}

# Program entry point / start here
if __name__ == '__main__':
    doc = libxml2.parseFile(input_file)
    ctxt = doc.xpathNewContext()

    res = ctxt.xpathEval("//record")
    for rec in res:
        model = rec.prop("model")
        id = rec.prop("id")
        if model not in nodes.keys():
            nodes[model] = {}
        nodes[model][id] = rec

    res = ctxt.xpathEval("//field")
    for rec in res:
        if rec.hasProp("ref"):
            ref = rec.prop("ref")
            name = rec.prop("name")
            if name == "group":
                if ref not in nodes["account.tax.group"].keys():
                    print("missing account.tax.group %s" % (ref,))
            if name == "tax" or name == "origin_tax":
                if ref not in nodes["account.tax.template"].keys():
                    print("missing account.tax.template %s" % (ref,))
            if name == "code":
                if ref not in nodes["account.tax.code.template"].keys():
                    print("missing account.tax.code.template %s" % (ref,))
            if name == "rule":
                if ref not in nodes["account.tax.rule.template"].keys():
                    print("missing account.tax.rule.template %s" % (ref,))
            if name == "parent":
                if ref not in nodes["account.tax.template"].keys() and ref not in nodes["account.tax.code.template"].keys():
                    print("missing parent %s" % (ref,))

    doc.freeDoc()
    ctxt.xpathFreeContext()
