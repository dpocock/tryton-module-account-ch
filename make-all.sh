#!/bin/bash

set -e

SUPPORTED_COUNTRIES="ch al"

./csv2l10nString.py > l10nStrings.xml

for TARGET_COUNTRY in $SUPPORTED_COUNTRIES ;
do
  ./make-account.py ${TARGET_COUNTRY}

  SUPPORTED_LANGUAGES=`ls ${TARGET_COUNTRY} | grep ^account_ | cut -f3 -d _ | cut -b1-2`
  for TARGET_LANG in $SUPPORTED_LANGUAGES ;
  do
    TAX_XML=${TARGET_COUNTRY}/tax_${TARGET_COUNTRY}_${TARGET_LANG}.xml
    saxonb-xslt -s:${TARGET_COUNTRY}/tax_${TARGET_COUNTRY}.xml -xsl:l10n.xsl -o:${TAX_XML} lang=${TARGET_LANG}
  done

done

