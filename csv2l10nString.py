#!/usr/bin/python3

import csv

csv_filename = 'l10nStrings.csv'

# Program entry point / start here
if __name__ == '__main__':
    header = []
    with open(csv_filename, "r") as csv_file:
        print("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
        print("<l10nStrings>")
        csv_reader = csv.reader(csv_file, delimiter=",")
        for row in csv_reader:
            if len(header) == 0:
                header = row
            else:
                col = 0
                id = None
                for val in row:
                    if id is None:
                        id = val
                        print("  <l10nString id=\"%s\">" % (id,))
                    else:
                        lang = header[col]
                        print("    <value lang=\"%s\">%s</value>" % (lang, val))
                    col = col + 1
                print("  </l10nString>")

        print("</l10nStrings>")
