
Value Added Tax Return (VAT)

1. The serial number is used by the Regional Tax Directorate and serves to update your records when the declaration is submitted.

2. The Tax Period is the month presented from the first day to the last day. All transactions (Sales, Purchases) and monthly VAT calculations performed during this period must be included in this declaration. Data of the Taxable Person.

3. Taxable Person Identification Number (NIPT / NUIS) is the only number that represents and identifies you in all relations with the Central Tax Administration. Nipt / Nuis must be placed on every invoice you issue or receive.

4. Trade Name of the Taxable Person is the trade name with which you are registered in the NRC or the Regional Tax Directorate according to the tax legislation in force and which is indicated on your registration certificate as NIPT / NUIS.

5. Name, Surname of a Natural Person is established when the economic activity is performed by a Natural Person and has no trade name.

6. The address is indicated for the main place of exercise of your economic activity. This address is also shown on your NIPT / NUIS certificate.

7. Telephone number (if you have one) shows the telephone number in the main place of exercise of your economic activity. Note. If any of the data in sections 4,5,6,7 have changed, you are obliged to declare in time, any change to the NRC or the Regional Tax Directorate where you are registered.

8. If you have not completed any transaction during the month, mark an "X" in box 8. According to the declaration method, fill in the date and sign declaring in this way that the information given in the declaration is complete and accurate. VAT calculated for the month

9. From the “Excluded Sales” column (ies) of your sales book, extract the total of all excluded sales of the month and fill in the amount obtained in box 9 of your Statement. VAT is not calculated for sales excluded by law.

10. From column (f) of the Sales Book "Sales without VAT" of your sales book, extract the total of all sales without VAT of the month and fill in the amount obtained in box 10 of your Declaration. VAT is not calculated for these sales.

11. From column (g) of the Sales Book "Exports of Goods" of your sales book extract the total of all exports or supplies with a tax rate of 0% made for the month and fill in the amount obtained in box 11 of your Declaration. Supplies Exports, according to article 57 of the law are taxed at a rate of 0%.

12. From the column (gj) of the Sales Book "Supplies with 0% rate" of your sales book extract the total of all exports or supplies with 0% tax rate made for the month and complete the amount obtained in box 10 of your Statement. Supplies according to article 59 of the Law "On VAT" (Exports, international transport, etc.) are taxed at 0%.

13. From column "h" of the Sales Book "Taxable value for sales at a rate of 20%" derive the total for transactions made during the month and fill in the amount obtained in box 11 of your Declaration. These taxable sales are subject to a 20% VAT rate. (value +/-)

14. From column "i" of the Sales Book "VAT for sales at a rate of 20%", deduct the total VAT for sales for the month at a rate of 20% and put the amount obtained in box 12 of your Declaration. The value in box 12 must be equal to 20% of the value shown in box 11. (value +/-)

15. From column “j” of the Sales Book “Taxable Value for Sale according to the regime of travel agents / profit margin / auction sale” “draw the total for the transactions made during the month and fill in the amount obtained in box 15 of the Declaration your. These taxable sales are subject to a 20% VAT rate.

16. From column "k" of the Sales Book "VAT for Sales regime travel agents / profit margin / auction sale", derive the total sales VAT for the month and the amount obtained put in box 16 of your Declaration. The value in box 16 must be equal to 20% of the value shown in box 15.

17. From column “l” of the Sales Book “Taxable Value for VAT Auto Loads on Sale” derive the total for the transactions for which VAT VAT Loads on sales made during the month are calculated and fill in the amount obtained in box 17 of your Declaration. These transactions are auto-charged with 20% VAT on sales.

18. From column "ll" of the Sales Book "VAT charged on sale", deduct the total VAT for transactions for which VAT is charged on sale for the month and the amount obtained put in box 18 of your Declaration. The value in box 18 must be equal to 20% of the value shown in box 17.

19. From box “m” of the Sales Book “Taxable Value for Bad Debt Actions” pull box 19 of your Declaration. Applicable VAT rate 20% (value +/-)

20. From column “n” of the Sales Book “VAT for bad Debt operations”, take out the total VAT for bad Debt collections for the month in installments and put the amount received in box 20 of your Declaration. The value in box 20 must be equal to 20% of the value presented in box 19. (value +/-)

21. Calculate the total VAT collected for the month, adding VAT calculated for Taxable Sales at 20% (box 14); VAT calculated for taxable sales according to the regimes of the travel agent / profit margin / auction sale, at 20% (box 16); ), VAT collected from bad Debt operations at a rate of 20% (box 20). Fill in the amount in box 21. (value +/-) VAT deductible during the Month

22. From the “ë” column of your Purchase Book - “Exempt purchases, with non-deductible VAT and without VAT”, extract the total of these purchases and fill in the amount obtained in box 22 of your Declaration. Excluded purchases will be registered here, with non-deductible VAT and without VAT, except for investment purchases which are registered separately in box 23. For purchases with non-deductible VAT will not be deducted VAT.

23. From column "f" of your Purchasing Book - "Purchase of investments within the country without VAT", Here are recorded only purchases made for investment purposes in the property of the entity made within the country and without VAT, such as. Excluded purchases, with non-deductible VAT and without VAT, for investments, extract the total of these purchases and fill in the amount obtained in box 23 of your Declaration.

24. From column "g" of your Purchasing Book - "Exempted investments without VAT", extract the total imports and fill in the amount obtained in box 24 of your Statement. Only imports made for investment purposes in the property of the entity are registered here.

25. From the “gj” column of your Purchasing Book - “Import excluded goods”, extract the total imports and fill in the amount obtained in box 25 of your Declaration. All imports of goods exempt from VAT will be registered here, except for imports exempted for investments which are registered separately in box 24.

26. From column “h” - “Taxable Value of Imports of goods at a rate of 20%” of your Purchase Book, for the month, derive the total taxable value of imports of goods at a rate of 20% and fill in the amount obtained, in box 26 of your Declaration. Only imports of goods with a rate of 20% are registered here, except for those made for investment purposes in the property of the subject, which are declared in box 28.

27. From column "i" - "VAT on Import for goods at a rate of 20%" of your Purchase Book, deduct the total VAT on import for non-investment goods and fill in the amount obtained in box 27 of your Declaration. The value in box 27 must be equal to 20% of the value of box 26.

28. From column "j" - "Taxable value of imports investment at a rate of 20%" of your Purchase Book, for the month, derive the total taxable value of imports at a reduced rate and fill in the amount obtained, in box 28 of your Declaration. Here are registered only the imports of goods with a rate of 20% performed for investment purposes in the property of the subject, which are declared in box 28.

29. From column "k" - "VAT on Import for investment at a rate of 20%" of your Purchase Book, deduct the total VAT on import for non-investment goods and fill in the amount obtained in box 27 of your Declaration. The value in box 29 must be equal to 20% of the value of box 28.

30. From column "l" - Taxable value from local suppliers at the rate of 20% "of your Purchase Book, extract the total of these purchases and fill in the amount obtained in box 30 of your Declaration. Here are recorded all purchases within the country except those made for investment purposes in the property of the entity that are declared in box 32. (value +/-)

31. From column "ll" - "VAT for purchases from local suppliers at a rate of 20%" of your Purchase Book, add the total of these purchases and fill in the amount obtained in box 31 of your Declaration. The value in box 31 should be equal to 20% of the value of box 30. (value +/-)

32. From column "m" -Taxable value for investment purchase from local suppliers at a rate of 20% "of your Purchase Book, extract the total of these purchases and fill in the amount obtained in box 32 of your Declaration. Only domestic purchases made for investment purposes in the subject's property are registered here (value +/-)

33. From column “n” - “VAT for investment purchases from local suppliers at a rate of 20%” of your Purchasing Book, collect the total of these purchases and fill in the amount obtained in box 33 of your Declaration. The value in box 33 must be equal to 20% of the value of box 32. (value +/-)

34. From column “nj” - “Taxable value of purchases from local farmers” of your Purchase Book, extract the total of these purchases from farmers identified with NIPT but not registered for VAT and fill in the amount obtained in the box 34 of your Statement. The rate applied by the buyer for these purchases is 20%.

35. From column "o" - "Deductible VAT, farmers' compensation" of your Purchase Book, add the total of these purchases and fill in the amount obtained in box 35 of your Declaration. The value in box 35 must be equal to 20% of the value of box 34.

36. From column “p” of the Purchasing Book “Taxable value for VAT self-loading on purchase” derive the total for transactions of purchase of services by non-resident persons in Albania for which VAT cargo on sale are calculated, made during the month and complete the amount obtained in box 36 of your Statement. The VAT rate applied is 20%.

37. From column "q" of the Purchase Book "VAT self-charged on purchase", derive the total VAT for transactions for which auto-charge VAT on purchase for the month is calculated and the amount obtained put in box 37 of your Declaration. The value in box 37 must be equal to 20% of the value shown in box 36.

38. From column “r” of the Purchase Book “Taxable Value for Deductible VAT Adjustment” derive the total for VAT adjustments during the month and fill in the amount obtained in box 38 of your Declaration. The VAT rate applied is 20%. (value +/-)

39. From the column "rr" of the Purchase Book "Regulated VAT", take out the total VAT for VAT adjustments for the month and put the amount received in box 39 of your Declaration. The value in box 39 must be equal to 20% of the value shown in box 38. (value +/-)

40. From column "s" of the Purchase Book "Taxable Value of Bad Debt" extract the total values ​​of bad debt transactions and record in box 40 of your Declaration Here are recorded bad debt transactions at a rate of 20% of VAT (value +/-)

41. From the “sh” column of the Purchase Book “VAT for bad Debt operations”, take out the total VAT for bad Debt billings for the month in installments and put the amount obtained in box 41 of your Declaration. The value in box 41 must be equal to 20% of the value presented in box 40. (value +/-)

42. Calculate the total deductible VAT for the month, by collecting deductible VAT on Imports of goods at a rate of 20% (box27); Deductible VAT on Investments at a rate of 20% (box29); Deductible VAT on Purchases from domestic suppliers at a rate of 20% (box 31), deductible VAT on Purchases for investments from domestic suppliers at a rate of 20% (box 33); VAT (compensation) paid on purchases by farmers (box 35). VAT calculated for transactions of purchase of services by non-resident persons self-loaded with purchase at 20% (box 37), VAT adjusted rate 20% according to box (39) VAT deductible from bad Debt operations at a rate of 20% ( box 41). Fill in the amount in box 42. (value +/-) VAT result for the month

43. Box 43 of the Declaration calculates the surplus of deductible VAT for the month, subtracting the value of box 21 from the value of box 42, if the total deductible VAT for the month (box 42) is greater than the VAT calculated for sales of of the month presented in box 21. So you have as a result Excess of deductible VAT for the month and you do not have to pay VAT in this month.

44. In box 44 of the Declaration is calculated the obligatory VAT to be paid by subtracting the value of box 21 from the value of box 42, if the amount of VAT calculated for the month with 20% tax rate, in box 21 of the declaration is more greater than the total deductible VAT for the month (box 42), so you have resulted in mandatory VAT to be paid. .

Notes: On completing the VAT Declaration
This statement is conceived based on Law no. 92/2014 dated 24.07.2014 "On VAT in the Republic of Albania" and is appropriate to perform the declaration of VAT according to the ways defined in the laws and bylaws. The declaration must be completed and declared electronically within the 14th day of the month following the month indicated in the declaration as a tax period.
Payment of VAT is made by Payment Order in the bank or electronically (if possible electronic payment) within the 14th of the month following the month indicated in the Declaration as a tax period. Payment orders will be automatically generated by the system after you have completed and submitted this statement. If you have payment to make and you make this payment in full within the deadline, no interest or late payment penalties will be applied to you within the payment value. If a partial payment or a late payment is made, your obligation under box 44 of this statement will be subject to the relevant fines and interest from the date the obligation was due until the date the payment is made.
Enter the filing date of the return, and send the return electronically, where a copy of this return will be stored in your account as a taxpayer.
For more information, read the laws and bylaws for VAT (Law and VAT Instruction, etc.) or contact the Regional Tax Directorate where you are registered.

