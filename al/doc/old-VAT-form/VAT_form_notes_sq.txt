Deklaratë për Tatimin mbi Vlerën e Shtuar (DTVSH)

1. Numri serial perdoret nga Drejtoria Rajonale Tatimore dhe sherben per te azhornuar regjistrimet tuaja kur paraqitet deklarata.

2. Periudha Tatimore është muaji i paraqitur nga dita e parë deri në ditën e fundit.Te gjitha transaksionet (Shitjet, Blerjet ) dhe llogaritjet mujore te TVSH-se te kryera gjate kesaj periudhe duhet te perfshihen ne kete deklaratë.  Të dhena të Personit të Tatueshem.

3. Numri Identifikues i Personit të Tatueshëm (NIPT/NUIS) është numri i vetem që ju perfaqeson dhe identifikon ne te gjitha marredheniet me Administraten Tatimore Qendrore. Nipt/Nuis duhet te vendoset detyrimisht ne çdo fature qe leshoni ose merrni.

4. Emri Tregtar i Personit te Tatueshem eshte emri tregtar me te cilin ju jeni regjistruar ne QKR ose Drejtorine Rajonale Tatimore sipas legjislacionit tatimor ne fuqi dhe qe tregohet ne certifikaten tuaj te regjistrimit si NIPT/NUIS.

5. Emri, Mbiemri i Personit Fizik vendoset kur veprimtaria ekonomike kryhet nga nje Person Fizik dhe nuk ka emer tregtar.

6. Adresa tregohet per vendin kryesor te ushtrimit te veprimtarise tuaj ekonomike. Kjo adrese eshte e treguar edhe ne certifikaten tuaj te NIPT/NUIS.

7. Numri i Telefonit (nqs keni) tregon numrin e telefonit ne vendin kryesor te ushtrimit te veprimtarise tuaj ekonomike.  Shenim. Nëqoftëse ndonjë nga të dhënat e rubrikave 4,5,6,7 ka ndryshuar, jeni të detyruar të deklaroni në kohë, çdo ndryshim pranë QKR-së ose Drejtorisë Rajonale Tatimore ku jeni i regjistruar.

8. Nëqoftëse ju nuk keni kryer asnjë transaksion gjatë muajit, shënoni një “X”në kutinë 8. Sipas mënyrës së deklarimit, plotësoni datën dhe firmosni duke deklaruar në këtë mënyrë që informacioni i dhënë në deklaratë është i plotë dhe i saktë.  TVSH e llogaritur për muajin

9. Nga kolona (ë) e Librit të shitjeve “Shitje të përjashtuara” e librit tuaj të shitjeve nxirrni totalin e të gjithë shitjeve të përjashtuara të muajit dhe plotësoni shumën e përftuar në kutinë 9 të Deklaratës tuaj. Për shitjet e përjashtuara sipas ligjit nuk llogaritet TVSH.

10. Nga kolona (f) e Librit të shitjeve “Shitje pa TVSH” e librit tuaj të shitjeve nxirrni totalin e të gjithë shitjeve pa TVSH të muajit dhe plotesoni shumen e perftuar ne kutinë 10 të Deklaratës tuaj.  Për këto shitje nuk llogaritet TVSH.

11.Nga kolona (g) e Librit të shitjeve “Eksporte mallrash” e librit tuaj të shitjeve nxirrni totalin e të gjithë eksporteve apo furnizimeve me shkallë tatimore 0% të bëra për muajin dhe plotesoni shumen e përftuar në kutinë 11 të Deklaratës tuaj. Furnizimet Eksportet, sipas nenit 57 te ligjit tatohen me shkalle 0%.

12.Nga kolona (gj) e Librit te shitjeve “Furnizime me shkallë 0%”e librit tuaj të shitjeve nxirrni totalin e të gjithë eksporteve apo furnizimeve me shkalle tatimore 0% të bëra për muajin dhe plote-soni shumen e përftuar në kutinë 10 të Deklaratës tuaj. Furnizimet sipas nenit 59 te Ligjit “Per TVSH” (Eksportet, transporti nderkombetar ,etj) tatohen me 0%.

13. Nga kolona “h” e Librit te shitjeve “Vlera e Tatueshme për shitje me shkallë 20%” nxirrni totalin per transaksionet e bera gjate muajit dhe plotesoni shumën e përftuar në kutinë 11 të Dek-laratës tuaj. Këto shitje të tatueshme i nënshtrohen shkalles për tvsh-në prej 20%. (vlera +/-)

14. Nga kolona “i” e Librit të shitjeve “TVSH për shitje me shkallë 20%”, nxirrni totalin e tvsh-se për shitjet për muajin me shkallë 20% dhe shumën e përftuar vendoseni në kutinë 12 të Deklaratës tuaj. Vlera në kutinë 12 duhet të jetë e barabartë me 20% të vlerës së paraqitur në kutinë 11.(vlera +/-)

15. Nga kolona “j” e Librit te shitjeve “Vlera e Tatueshme per Shitje sipas regjimit te agjenteve te udhetimit/ marzhi fitimit /shitje ne ankand”“ nxirrni totalin per transaksionet e bera gjate muajit dhe plotesoni shumën e përftuar në kutinë 15 të Deklaratës tuaj. Keto shitje te tatueshme i nënshtrohen shkalles 20% të TVSH-se.

16. Nga kolona “k” e Librit te shitjeve “TVSH për Shitje regjimi agjentëve të udhëtimit/ marzhi fitimit /shitje ne ankand ”, nxirrni totalin e tvsh-se te shitjeve për muajin dhe shumën e përftuar vendoseni në kutinë 16 të Deklaratës tuaj. Vlera në kutinë 16 duhet të jete e barabarte me 20 % te vleres se paraqitur në kutinë 15.

17. Nga kolona “l” e Librit të shitjeve “Vlera e Tatueshme për Autongarkese TVSH ne shitje” nxirrni totalin per transaksionet për të cilat llogariten autongarkese TVSH në shitje te bera gjate muajit dhe plotesoni shumën e përftuar në kutinë 17 të Deklaratës tuaj. Keto transaksione auton-garkohen me 20 % TVSH në shitje.

18. Nga kolona “ll” e Librit të shitjeve “TVSH e autongarkuar në shitje ”, nxirrni totalin e tvsh-se për transaksionet për të cilat llogariten autongarkese TVSH në shitje per muajin dhe shumën e përftuar vendoseni në kutinë 18 të Deklaratës tuaj. Vlera në kutinë 18 duhet të jetë e barabarte me me 20% të vlerës së paraqitur në kutinë 17.

19. Nga kolona “m” e Librit të shitjeve “Vlera e Tatueshme për veprime te Borxhit e keq” nxirrni kutine 19 të Deklaratës tuaj. Shkalla e zbatueshme e TVSH-se 20% .(vlera +/-)

20. Nga kolona “n” e Librit të shitjeve “TVSH për veprime të Borxhit e keq”, nxirrni totalin e tvsh-se per arketimet e Borxhit te keq për muajin me shkalle dhe shumën e përftuar vendoseni në kutinë 20 të Deklaratës tuaj. Vlera në kutinë 20 duhet të jete e barabarte me me 20% të vlerës së paraqitur në kutinë 19. (vlera +/-)

21. Llogarisni totalin e TVSH së mbledhshme për muajin, duke mbledhur TVSH e llogaritur për Shitje të tatueshme me 20% (kutia14); TVSH e llogaritur për shitje të tatueshme sipas regjimeve te agjentit udhetimi/marzhit fitimit/ shitje ne ankand, me 20 % (kutia16);TVSH e llogaritur për transaksionet e blerjes se shërbimeve nga persona jo rezident e autongarkuar në shitje me 20 % (kutia 18), TVSH e mbledhshme nga veprime te Borxhit të keq me shkallë 20% (kutia 20). Shumën plotësojeni në kutinë 21. (vlera +/-) TVSH e zbritshme gjatë Muajit

22. Nga kolona “ë” e Librit tuaj të Blerjeve-”Blerje të përjashtuara, me TVSH jo të zbritshme dhe pa tvsh” , nxirrni totalin e këtyre blerjeve dhe plotësoni shumën e përftuar në kutinë 22 të Deklaratës tuaj. Këtu do të regjistrohen blerjet e përjashtuara, me TVSH jo të zbritshme dhe pa tvsh, përveç blerjeve për investime të cilat regjistrohen veçmas në kutinë 23. Për blerjet me tvsh jo të zbritshme nuk do të bëhet zbritje e TVSH.

23. Nga kolona “f”e Librit tuaj të Blerjeve-”Blerje investime brenda vendit pa TVSH” ,Ketu regjistrohen vetem blerjet e kryera për qëllime investimi në pasurinë e subjektit te kryera brenda vendit dhe pa TVSH, si p.sh. blerjet e përjashtuara, me TVSH jo të zbritshme dhe pa tvsh, per investime nxirrni totalin e këtyre blerjeve dhe plotesoni shumën e përftuar në kutinë 23 të Deklaratës tuaj.

24. Nga kolona “g”e Librit tuaj të Blerjeve-”Importe të përjashtuara të investimit pa TVSH”, nxirrni totalin e importeve dhe plotesoni shumën e përftuar në kutinë 24 të Deklaratës tuaj. Këtu regjistrohen vetëm importet e kryera për qëllime investimi në pasurinë e subjketit.

25. Nga kolona “gj”e Librit tuaj të Blerjeve-”Import mallra të përjashtuara”, nxirrni totalin e importeve dhe plotesoni shumën e përftuar në kutinë 25 të Deklaratës tuaj. Këtu do te regjistrohen te gjitha importet e mallrave të përjashtuara nga TVSH, përveç importeve te perjashtuara per investime të cilat regjistrohen veçmas në kutinë 24.

26. Nga kolona “h”-“Vlera e Tatueshme e Importe mallra me shkallë 20%” e Librit tuaj të Blerjeve, për muajin, nxirrni totalin e vleres se tatueshme te importieve te mallrave me shkalle 20% dhe plotesoni shumën e përftuar, ne kutinë 26 të Deklaratës tuaj.Ketu regjistrohen vetem importet e mallrave me shkalle 20%, pervec atyre te kryera për qellime investimi ne pasurine e subjektit, te cilat deklarohen ne kutine 28.

27. Nga kolona “i”- “ TVSH ne Import per mallra me shkalle 20%“ e Librit tuaj të Blerjeve,nxirrni totalin e TVSH-se ne import per mallrat jo te investimit dhe plotesoni shumën e përftuar në kutinë 27 të Deklaratës tuaj. Vlera në kutinë 27 duhet të jetë e barabartë me 20% te vlerës së kutisë 26.

28. Nga kolona “j”-“Vlera e Tatueshme e Importe investimi me shkallë 20%” e Librit tuaj të Blerjeve, për muajin, nxirrni totalin e vleres se tatueshme te importit me shkalle te reduktuar dhe plotesoni shumën e përftuar, ne kutinë 28 të Deklaratës tuaj.Ketu regjistrohen vetem importet e mallrave me shkalle 20% te kryera për qëllime investimi në pasurinë e subjektit, te cilat deklarohen në kutinë 28.

29. Nga kolona “k”- “TVSH në Import për investimi me shkallë 20%“ e Librit tuaj të Blerjeve, nxirrni totalin e TVSH-se ne import per mallrat jo te investimit dhe plotesoni shumën e përftuar në kutinë 27 të Deklaratës tuaj. Vlera në kutinë 29 duhet të jetë e barabartë me 20% te vlerës së kutisë 28.

30. Nga kolona “l“-Vlera e tatueshme nga furnitore vendas me shkalle 20%” e Librit tuaj të Blerjeve, nxirrni totalin e këtyre blerjeve dhe plotesoni shumën e përftuar në kutinë 30 të Deklaratës tuaj. Këtu regjistrohen të gjitha blerjet brenda vendit pervec atyre te kryera për qëllime investimi në pasurinë e subjektit që deklarohen në kutine 32.(vlera +/-)

31. Nga kolona “ll”- “TVSH për blerje nga furnitorë vendas me shkallë 20%” e Librit tuaj të Blerjeve, mblidhni totalin e këtyre blerjeve dhe plotesoni shumën e përftuar në kutinë 31 të Deklaratës tuaj.Vlera në kutinë 31 duhet të jetë e barabartë me 20% të vlerës së kutisë 30.(vlera +/-)

32. Nga kolona “m“-Vlera e tatueshme per blerje te investimit nga furnitore vendas me shkallë 20%” e Librit tuaj të Blerjeve, nxirrni totalin e këtyre blerjeve dhe plotesoni shumën e përftuar në kutinë 32 të Deklaratës tuaj. Këtu regjistrohen vetëm blerjet brenda vendit te kryera për qëllime investimi në pasurinë e subjektit.(vlera +/-)

33. Nga kolona “n”- “TVSH per blerje te investimit nga furnitore vendas me shkallë 20%” e Librit tuaj të Blerjeve, mblidhni totalin e këtyre blerjeve dhe plotesoni shumën e përftuar në kutinë 33 të Deklaratës tuaj. Vlera në kutinë 33 duhet të jetë e barabartë me 20% të vlerës së kutisë 32.  (vlera +/-)

34. Nga kolona “nj”-“Vlera e tatueshme e blerjeve nga fermerët vendas” e Librit tuaj të Blerjeve,nxirrni totalin e këtyre blerjeve nga fermerët e identifikuar me NIPT por jo te regjistruar per TVSH-ne dhe plotesoni shumën e përftuar në kutinë 34 të Deklaratës tuaj. Shkalla e zbatuar nga blerësi për këto blerje është 20%.

35. Nga kolona “o”-“TVSH i zbritshëm, kompensimi i fermerëve” e Librit tuaj të Blerjeve, mblidhni totalin e këtyre blerjeve dhe plotësoni shumën e përftuar në kutinë 35 të Deklaratës tuaj.  Vlera në kutinë 35 duhet të jetë e barabartë me 20 % të vlerës së kutisë 34.

36. Nga kolona “p” e Librit te blerjeve “Vlera e Tatueshme për Autongarkesë TVSH në blerje” nxirrni totalin për transaksionet e blerjes së shërbimeve nga persona jo rezident ne Shqipëri për të cilat llogariten autongarkese TVSH në shitje ,te bëra gjatë muajit dhe plotësoni shumën e përftuar në kutinë 36 të Deklaratës tuaj. Shkalla e TVSH-se e zbatuar eshte 20%.

37. Nga kolona “q” e Librit të blerjeve “TVSH e autongarkuar ne blerje ”, nxirrni totalin e tvsh-se për transaksionet për të cilat llogariten autongarkese TVSH në blerje per muajin dhe shumën e përftuar vendoseni në kutinë 37 të Deklaratës tuaj. Vlera në kutinë 37 duhet të jetë e barabartë me 20% të vlerës së paraqitur në kutinë 36.

38. Nga kolona “r” e Librit të blerjeve “Vlera e Tatueshme për Rregullim të TVSH së zbritshme” nxirrni totalin per rregullimet e TVSH-se gjate muajit dhe plotesoni shumën e përftuar në kutinë 38 të Deklaratës tuaj. Shkalla e TVSH-se e zbatuar eshte 20%. (vlera +/-)

39. Nga kolona “rr” e Librit te blerjeve “TVSH e Rregulluar ”, nxirrni totalin e tvsh-së për rregullime të TVSH për muajin dhe shumën e përftuar vendoseni në kutinë 39 të Deklaratës tuaj. Vlera në kutinë 39 duhet të jete e barabarte me me 20% të vlerës së paraqitur në kutinë 38.(vlera +/-)

40. Nga kolona “s” e Librit te blerjese “Vlera e Tatueshme të Borxhit e keq” nxirrni totalin e vlerave te transaksioneve të borxhit të keq dhe regjistrojeni në kutinë 40 të Deklaratës tuaj Ketu regjistrohen transaksione te borxhit te keq me shkallë 20% te TVSH-së.(vlera +/-)

41. Nga kolona “sh” e Librit te blerjeve “TVSH për veprime të Borxhit e keq”, nxirrni totalin e tvsh-së për faturimet e Borxhit të keq për muajin me shkallë dhe shumën e përftuar vendoseni në kutinë 41 të Deklaratës tuaj. Vlera në kutinë 41 duhet të jete e barabarte me me 20% të vlerës së paraqitur në kutinë 40.(vlera +/-)

42. Llogarisni totalin e TVSH së zbritshëm për muajin, duke mbledhur TVSH e zbritshëm për Importet mallra me shkalle 20%(kutia27);TVSH e zbritshëm për Importet te investimeve me shkalle 20%(kutia29);TVSH e zbritshëm për Blerjet nga furnizuesit vendas me shkalle 20%(kutia 31), TVSH e zbritshëm për Blerjet per investime nga furnizuesit vendas me shkalle 20%(kutia 33); TVSH (kompensimi) i paguar për blerjet nga fermerët (kutia 35). TVSH e llogaritur për transkasionet e blerjes se shërbimeve nga persona jo resident e autongarkuar ne blerje me 20% (kutia 37), TVSH e rregulluar shkalle 20% siaps kutise (39) TVSH e zbritshme nga veprime te Borxhit të keq me shkallë 20% (kutia 41).Shumën plotësojeni në kutinë 42 . (vlera +/-) Rezultati i TVSH-se për muajin

43. Në kutine 43 të Deklaratës llogaritet Tepricë e TVSH së zbritshme per muajin, zbritur vlerën e kutisë 21 nga vlera e kutise 42, nqs totali i TVSH të zbritshëm për muajin (kutia 42) është më i madh se TVSH i llogaritur për shitjet e muajit te paraqitura ne kutinë 21. Pra keni si rezultat Tepricë të TVSH së zbritshme per muajin dhe nuk duhet të bëni pagesë tvsh në këtë muaj.

44.Në kutine 44 të Deklaratës llogaritet TVSH e detyrueshëm për tu paguar duke zbritur vlerën e kutisë 21 nga vlera e kutise 42, nqs shuma e TVSH-se se llogaritur për muajin me shkalle tatimore 20%, ne kutine 21 te deklarates është më i madh se totali i TVSH të zbritshme për muajin (kutia 42), pra keni rezultuar me TVSH të detyrueshme për tu paguar. .

Shënime : Mbi plotësimin e Deklaratës së TVSH– së
Kjo deklarate eshte konceptuar ne baze të Ligjit Nr. 92/2014 date 24.07.2014 “Për TVSH-në në Republikën e Shqipërisë” dhe është e përshtatshme për të kryer deklarimin e TVSH-së sipas mënyrave të percaktuara ne aktet ligjore dhe nenligjore. Deklarata duhet të plotësohet dhe deklarohet ne menyre elektronike brenda dates 14 te muajit pasardhes te muajit te treguar ne deklarate si periudhe tatimore.
Pagesa e TVSH-se behet me Urdher pagese ne banke ose ne menyre elektronike (nese eshte e mundur pagesa elektronike) brenda datës 14 të muajit pasardhës te muajit te treguar në Deklaratë si periudhë tatimore. Urdherpagesa do t’ju gjenerohet automatikisht nga sistemi pasi te keni plotesuar dhe derguar kete deklarate. Nqs ju keni pagesë për të bërë dhe e bëni këtë pagesë te plote brenda afatit nuk ju aplikohen kamatevonesa ose gjoba per mospagese ne afat te vleres se pageses. Nqs bëhet një pagesë e pjesshme ose një pagesë e vonuar, detyrimit tuaj sipas kutise 44 te kesaj deklarate i shtohen gjobat dhe interesat perkatese nga data qe detyrimi duhej paguar deri ne daten kur kryhet pagesa.
Vendosni datën e plotësimit të Deklaratës, dhe dergojeni deklaraten ne menyre elektronike, ku një kopje e kësaj deklarate do t’ju ruhet ne llogarine tuaj si tatimpagues.
Për më shumë informacion lexoni aktet ligjore dhe nenligjore per TVSH-ne, (Ligjin dhe Udhëzimin e TVSH– se etj) ose merrni kontakt me Drejtorine Rajonale Tatimore ku jeni i regjistruar.

