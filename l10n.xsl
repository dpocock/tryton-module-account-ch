<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <!-- target language must be specified by the calling application -->
  <xsl:param name="lang"/>

  <!-- we load a copy of the l10n database -->
  <xsl:param name="l10nStrings" select="document('l10nStrings.xml')"/>

  <xsl:template match="placeholder">
    <xsl:param name="args"/>
    <xsl:param name="pos" select="@pos"/>
    <xsl:value-of select="$args[@pos=$pos]/@val"/>
  </xsl:template>

  <xsl:template match="l10nStringRef">
    <xsl:variable name="stringId" select="@id"/>
    <xsl:variable name="args" select="arg"/>
    <xsl:for-each select="$l10nStrings//l10nString[@id=$stringId]/value[@lang=$lang]">
      <xsl:apply-templates>
        <xsl:with-param name="args" select="$args"/>
      </xsl:apply-templates>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="@ref[parent::field]">
    <xsl:attribute name="ref">
      <xsl:value-of select="concat(../@ref, '_', $lang)"/>
    </xsl:attribute>
  </xsl:template>

  <xsl:template match="@id[parent::record]">
    <xsl:attribute name="id">
      <xsl:value-of select="concat(../@id, '_', $lang)"/>
    </xsl:attribute>
  </xsl:template>

  <xsl:template match="data">
    <xsl:copy>
      <xsl:attribute name="language" select="$lang"/>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

</xsl:transform>
