#!/bin/bash

set -e

if [ $# -lt 1 ];
then
  echo "Please specify country code"
  exit 1
fi

COUNTRY_CODE=$1

echo Deploying country: $COUNTRY_CODE

MODULE_DIR=${DESTDIR}/usr/lib/python3/dist-packages/trytond/modules/account_${COUNTRY_CODE}

mkdir -p ${MODULE_DIR}

cp ${COUNTRY_CODE}/tax_${COUNTRY_CODE}_??.xml ${MODULE_DIR}/ || echo missing tax_ files, ignored
cp ${COUNTRY_CODE}/account_${COUNTRY_CODE}_??.xml ${MODULE_DIR}/
cp ${COUNTRY_CODE}/tryton.cfg ${MODULE_DIR}




